import React from 'react';
import './App.css';
import { Layout, Menu } from 'antd';
import { CopyrightOutlined } from '@ant-design/icons';
import VideoPlayer from './components/VideoPlayer';

const { Header, Content, Footer } = Layout;

function App() {
  return (
    <>
      <Layout>
        <Header style={{ zIndex: 1, width: '100%', height: '7vh' }}>
          <div className=".App-logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">Home</Menu.Item>
            <Menu.Item key="2">Project</Menu.Item>
            <Menu.Item key="3">About Us</Menu.Item>
          </Menu>
        </Header>
        <Content>
          {/* <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb> */}
          <div style={{ pointerEvents: 'none', overflow: 'hidden' }}>
            <VideoPlayer url='https://youtu.be/zXbrHhKL7u8' scale={1.4} width='100vw' height='85vh' autoplay={true} loop={true} controls={false} muted={true} />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center', fontWeight: "bold", height:'8vh' }}>Copyright <CopyrightOutlined /> 2020 Titan Interactive Entertainment</Footer>
      </Layout>
    </>
  );
}

export default App;
