import React from 'react'
import ReactPlayer from 'react-player/lazy'

export default function VideoPlayer(props) {

    const videoSource = props.url

    return (
        <>
            <ReactPlayer style={{ transform: `scale(${props.scale})` }} width={props.width} height={props.height} url={videoSource} playing={props.autoplay} loop={props.loop} controls={props.controls} muted={props.muted} />
        </>
    )
}
